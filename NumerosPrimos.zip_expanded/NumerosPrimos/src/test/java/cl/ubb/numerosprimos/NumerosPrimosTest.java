package cl.ubb.numerosprimos;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumerosPrimosTest {
    @Test
    public void GeneraListaNumerosPrimosHastaCero(){
    	
    	/*arrange*/
    	List<Integer> lista = new ArrayList<Integer>();
    	List<Integer> listaEsperada = new ArrayList<Integer>();
    	listaEsperada.add(0);
    	
    	/*act*/
         lista= NumerosPrimos.generarLista(0);
    	
    	/*assert*/
    	assertEquals(listaEsperada, lista);
    	
    }
	
	/*  */
	/*@Test
	public void IngresarNumeroCero() {
		
		/*Arrange
		
		NumerosPrimos np = new NumerosPrimos();
		int resultado;
		
		/*Act
		resultado = np.juego(1);
				
		/*Assert
		assertEquals(resultado,"0");
	
	} */

}
